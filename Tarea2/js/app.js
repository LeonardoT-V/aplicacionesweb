class Cliente {

    constructor(nombre, apellido, ci, direccion, telefono, celular, correo, nacimiento) {
        this._nombre = nombre;
        this._apellido = apellido;
        this._ci = ci;
        this._direccion = direccion;
        this._telefono = telefono;
        this._celular = celular;
        this._correo = correo;
        this._nacimiento = nacimiento;
    }
    mostrarInformacion() {
        alert(`Se ha registrado el cliente ${this._nombre.toUpperCase()} ${this._apellido.toUpperCase()}
        Con la cedula ${this._ci}. 
        Reside en ${this._direccion}.
        Con los siguientes medios de contacto:
            -Celular ${this._celular}. 
            -Telefonico ${this._telefono}.
            -Correo electronico ${this._correo}.
        Con fecha de nacimiento el ${this._nacimiento}.
        `)
    }
}


function recibirFormulario() {
    let formulario = document.forms["formulario-global"];
    let nombre = formulario["nombre"].value;
    let apellido = formulario["apellido"].value;
    let cedula = formulario["cedula"].value;
    let direccion = formulario["direccion"].value;
    let telefono = formulario["telefono"].value;
    let celular = formulario["celular"].value;
    let correo = formulario["correo"].value;
    let nacimiento = formulario["nacimiento"].value;

    let a = new Cliente(nombre, apellido, cedula, direccion, telefono, celular, correo, nacimiento);
    a.mostrarInformacion();
}
